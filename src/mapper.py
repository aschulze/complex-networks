import matplotlib.pyplot as plt
from sklearn import cluster
import kmapper as km
import numpy as np


def DSGA_mapper(L1Dcmat, path_html, title, labels=None, p=2, k=4, eps=0.5, min_samples=0, intervals=15, perc_overlap=0.8, verbose=0):
    """Mapper for DSGA transformed data.
    ----------
    L1Dcmat: array
        DSGA transformed data.
    path_html: str
        Html file name for the results.
    title: str
        Title of the plotted results.
    labels: atr array
        Labels of all samples. Used in the tooltip.
    p: int
        L^p norm in projection step.
    k: int
        k-th power of L^p norm in projection step.
    eps: dbl
        Eps of clustering.
    min_samples: int
        Exclude all clusters with less samples.
    intervals: int
        Number open sets for the cover.
    perc_overlap: dbl
        Overlap percentage of the covering intervals. [0,1]
    verbose: int
        Sets the verbosity of the mapper class. 0 means no logging and 3 maximal logging.
    Returns
    -------
    """
    m = km.KeplerMapper()

    # k-th power of the L^p norm of each column.
    lens = np.power(np.sum(np.power(np.abs(L1Dcmat), p), axis=0), k/p)
    cl = cluster.DBSCAN(eps=eps, metric='correlation', min_samples=min_samples)
    co = km.Cover(n_cubes=intervals, perc_overlap=perc_overlap)

    graph = m.map(lens, L1Dcmat.T, clusterer=cl, cover=co)

    m.visualize(graph, path_html=path_html, title=title, custom_tooltips=labels)
    km.draw_matplotlib(graph)
    plt.show()
