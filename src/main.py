import data
import DSGA
import mapper
import numpy as np


def mapper_GSE45827():
    basal, her, luminala, luminalb, normal, labels = data.load_GSE45827_data()
    tumor = np.concatenate([basal, her, luminala, luminalb], axis=1)
    L1Dcmat = DSGA.DSGA_transform(N=normal, T=tumor)
    mapper.DSGA_mapper(L1Dcmat, "../results/GSE45827.html", "GSE45827", labels, p=2,
                       k=4, eps=0.4, min_samples=3, intervals=15, perc_overlap=0.4, verbose=0)


def mapper_GSE70947():
    tumor, normal, labels = data.load_GSE70947_data()
    L1Dcmat = DSGA.DSGA_transform(N=normal, T=tumor)
    mapper.DSGA_mapper(L1Dcmat, "../results/GSE70947.html", "GSE70947", labels, p=2,
                       k=1, eps=1.0, min_samples=3, intervals=30, perc_overlap=0.4, verbose=0)


def mapper_GSE89116():
    early, late, normal, labels = data.load_GSE89116_data()
    tumor = np.concatenate([early, late], axis=1)
    L1Dcmat = DSGA.DSGA_transform(N=normal, T=tumor)
    mapper.DSGA_mapper(L1Dcmat, "../results/GSE89116.html", "GSE89116", labels, p=2,
                       k=4, eps=0.8, min_samples=0, intervals=10, perc_overlap=0.6, verbose=0)


def plot_disease_comp_histogram_GSE45827():
    basal, her, luminala, luminalb, normal, _ = data.load_GSE45827_data()
    tumor = np.concatenate([basal, her, luminala, luminalb], axis=1)
    DSGA.disease_comp_histogram(
        N=normal, T=tumor, dim=6, filename="../results/disease_comp_histogram_GSE45827.pdf")


def plot_Wold_invariants_GSE45827():
    _, _, _, _, normal, _ = data.load_GSE45827_data()
    N_hat = DSGA.flat_construction(normal)
    u, s, vh = np.linalg.svd(N_hat, full_matrices=False)
    DSGA.plot_goodness(
        s, N_hat.shape[0], filename="../results/Wold_Invariants_GSE45827.pdf")


def main():
    # %% Applying DSGA and mapper to different data sets and storing the interactive plot in "results" folder.
    mapper_GSE45827()
    # mapper_GSE70947() # very slow
    mapper_GSE89116()

    # %% Plotting and storing the wold invariants and histogram of the disease components of GSE45827.
    plot_disease_comp_histogram_GSE45827()
    plot_Wold_invariants_GSE45827()


if __name__ == "__main__":
    main()
