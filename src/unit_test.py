import numpy as np
import pytest

import DSGA


def test_flat_construction():
    # The first three columns can be expressed as a linear combination of the other columns.
    # For the last one this is not possible.
    A = np.array([[1, 2, 3, 1],
                  [2, 3, 4, 3],
                  [3, 4, 5, 2],
                  [4, 5, 6, 7]])

    A_flat = DSGA.flat_construction(A)
    diff = A - A_flat
    assert np.allclose(diff[:, :3], np.zeros((4, 3)))
    assert not np.allclose(diff[:, 3], np.zeros((4, 1)))


def test_goodness():
    # Matrix has rank 3. Thus the goodness should have a local maximum at 3.
    # Note that the first and last value are always high and hence, are not considered.
    A = np.array([[1, 1, 1, 1, 1, 1, 1],
                  [2, 2, 2, 2, 2, 5, 6],
                  [3, 3, 3, 3, 3, 2, 2],
                  [4, 4, 4, 4, 4, 65, 235],
                  [5, 5, 5, 5, 5, 4, 35],
                  [6, 6, 6, 6, 6, 23, 2],
                  ])

    u, s, vh = np.linalg.svd(A, full_matrices=False)
    W = DSGA.goodness(s, A.shape[0])

    assert W.size == s.size - 1
    assert np.argmax(W[1:-1]) == 1


def test_HSM():
    # Test if the dimension reduction works (rank reduction).
    A = np.array([[1, 1, 1, 1, 1, 1, 1],
                  [2, 2, 2, 2, 2, 5, 6],
                  [3, 3, 3, 3, 3, 2, 2],
                  [4, 4, 4, 4, 4, 65, 235],
                  [5, 5, 5, 5, 5, 4, 35],
                  [6, 6, 6, 6, 6, 23, 2],
                  ])

    hsm2 = DSGA.HSM(A, dim=2)
    hsm1 = DSGA.HSM(A, dim=1)

    # shape and rank
    assert np.linalg.matrix_rank(A) == 3
    assert np.linalg.matrix_rank(hsm2) == 2
    assert np.linalg.matrix_rank(hsm1) == 1


def test_disease_comp():
    # healthy data
    N = np.array([[1.0, 0.0],
                  [0.0, 1.0],
                  [0.0, 0.0]])

    # sample data
    T = np.array([[1.0, 1.0, 1.0],
                  [1.0, 2.0, 2.0],
                  [0.0, 0.0, 1.0]])

    # Only the third column/ sample of T has a disease component.
    solution = np.array([[0.0, 0.0, 0.0],
                         [0.0, 0.0, 0.0],
                         [0.0, 0.0, 1.0]])

    T_hat = DSGA.disease_comp(N=N, T=T)

    assert np.array_equal(T_hat, solution)


def test_significant_genes():
    # Significant genes suffices the relaxed threshold and are correlated to at least three
    # genes sufficing the stringent threshold.
    Dcmat = np.array([[1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [1, 1, -1, 0],  # -, correlated
                      [5, -5, 5, 0],  # relaxed, correlated
                      [15, -15, 15, 0],  # stringent, correlated
                      [15, -15, 15, 0],  # stringent, correlated
                      [15, -15, 15, 0],  # stringent, correlated
                      [15, 15, 0, 15], ])  # stringent, not correlated

    solution = np.array([[5, -5, 5, 0],
                         [15, -15, 15, 0],
                         [15, -15, 15, 0],
                         [15, -15, 15, 0], ])

    sign_ind = DSGA.significant_genes(Dcmat)

    assert np.array_equal(Dcmat[sign_ind], solution)
