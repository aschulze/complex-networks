import pandas as pd
from numpy import append, empty


def load_samples(tbl, keys, columns=["samples", "type"]):
    """Loads GSE45827 CUMIDA data.
    ----------
    tbl: DataFrame
        One sample per row. First columns are "samples and "type".
    keys: str list
        Types of sample data to extract.
    columns: str list
        Column names of the samples/ids and types/diagnosis.
    array
        Labels of all samples.
    Returns
    -------
    tuple of arrays
        Arrays for each sample type, without "samples" and "type" data.
        Each column contains one sample.
    """
    samples = []
    labels = empty(0)
    for key in keys:
        ind = tbl.iloc[:, 1] == key
        sub_tbl = tbl.loc[ind, :]
        samples.append(sub_tbl.drop(columns=columns).to_numpy().T)
        labels = append(labels, sub_tbl[columns[1]].to_numpy())

    return *tuple(samples), labels


def load_GSE45827_data():
    """Loads GSE45827 CUMIDA data.
    ----------
    Returns
    -------
    array
        Microarray data of pathological samples. Each column represents a sample.
        41 basal type.
    array
        Microarray data of pathological samples. Each column represents a sample.
        30 HER2 type.
    array
        Microarray data of pathological samples. Each column represents a sample.
        29 luminal a type.
    array
        Microarray data of pathological samples. Each column represents a sample.
        30 luminal b type.
    array
        Microarray data of healthy samples. Each column represents a sample. (7 samples)
    array
        Labels of all samples.
    """

    tbl = pd.read_csv('../data/breast-cancer-gene-expression-cumida/Breast_GSE45827.csv',
                      sep=",", low_memory=False)
    samples = load_samples(
        tbl, ["basal", "HER", "luminal_A", "luminal_B", "normal"])

    return samples


def load_GSE70947_data():
    """Loads GSE70947 CUMIDA data.
    ----------
    Returns
    -------
    array
        Microarray data of 195 pathological samples (adenocarcinomas).
    array
        Microarray data of 195 adjacent, healthy samples.
    array
        Labels of all samples.
    """
    tbl = pd.read_csv('../data/breast-cancer-gene-expression-cumida/Breast_GSE70947.csv',
                      sep=",", low_memory=False)
    samples = load_samples(tbl, ["breast_adenocarcinoma", "normal"])

    return samples


def load_GSE89116_data():
    """Loads GSE89116 CUMIDA data.
    ----------
    Returns
    -------
    array
        Microarray data of early onset tumors (<=40years)
    array
        Microarray data of late onset tumors (>=55years)
    array
        Microarray data of adjacent, healthy samples.
    array
        Labels of all samples.
    """
    tbl = pd.read_csv('../data/breast-cancer-gene-expression-cumida/Breast_GSE89116.csv',
                      sep=",", low_memory=False)
    samples = load_samples(tbl, ["tumoral_early", "tumoral_late", "normal"])

    return samples
