from matplotlib.ticker import MaxNLocator
import matplotlib.pyplot as plt
import numpy.linalg as nla
import numpy as np
import seaborn as sns


def flat_construction(N):
    """Flat construction. Fit each column to a linear model in all other columns.
    ----------
    N : array
        Each column contains a normal tissue sample.
    Returns
    -------
    array
        Flat tissue data.
    """

    N_hat = np.zeros(N.shape)

    for i in range(N.shape[1]):
        A = nla.lstsq(np.delete(N, i, 1), N[:, i, None], rcond=None)[0]
        N_hat[:, i] = np.dot(np.delete(N, i, 1), A).flatten()

    return N_hat


def goodness(s, n):
    """Wold invariants to measure goodness of fit for PCA.
    ----------
    s: array
        Singular values of a matrix.
    n : integer
        Number of rows/ data points per sample.
    Returns
    -------
    array
        Wolds invariants.
    """
    R = len(s)
    W = np.empty(s.size - 1)

    for l in range(W.size):
        W[l] = (
            s[l]
            * s[l]
            / np.sum(s[l + 1:] * s[l + 1:])
            * (n - l - 1)
            * (R - l)
            / (n + R - 2 * l)
        )

    return W


def plot_goodness(s, n, filename=""):
    """Plots Wold invariants to measure goodness of fit for PCA.
    ----------
    s: array
        Singular values of a matrix.
    n : integer
        Number of rows/ data points per sample.
    filename: str
        If not the empty string, the plot will be stored under the given file name.
    Returns
    -------
    """
    W = goodness(s, n)

    ax = plt.figure().gca()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_yscale('log')
    plt.title("Wold Invariants of the Flat Normal Tissue Data")
    plt.xlabel("Dimension")
    plt.ylabel("W")
    plt.plot(np.arange(1, W.shape[0]+1), W)

    if not filename == "":
        plt.savefig(filename, bbox_inches='tight')

    plt.show()


def HSM_get_input(s_size):
    """Gets user input for the dimensionality reduction.
    ----------
    s_size: int
        Number of singular values used in the Wold invariant.
    Returns
    -------
    int
        Dimension for reduction.
    """
    L = int(input("Please enter the dimension for the Principal Component Analysis (PCA),"
                  "i.e, enter a local maximum of the Wold invariants."))

    assert L >= 1 and L < s_size, "The input dimension has to lie in [1," + str(
        s_size) + ")."

    return L


def HSM(N, dim=None):
    """Compute the Healthy State Model (HSM) by means of a Modified Principal Component
    Analysis and the flat construction.
    ----------
    N: array
        Each column contains a normal tissue sample.
    dim: int
        If None, the goodness will be plotted and the user has to input the local maximum of the
        Wold invariant.
    Returns
    -------
    array
        Dimension reduced N.
    """
    N_hat = flat_construction(N)
    u, s, vh = np.linalg.svd(N_hat, full_matrices=False)

    if dim == None:
        # The user has to input the PCA dimension because there is no stable algorithm to compute it.
        plot_goodness(s, N_hat.shape[0])
        L = HSM_get_input(len(s))
    else:
        L = dim

    s[L:] = 0.0
    N_hat_L = u @ np.diag(s) @ vh

    return N_hat_L


def disease_comp(N, T):
    """Compute disease component from sample data using normal tissue data.
    ----------
    N: array
        Each column contains a normal tissue sample.
    T: array
        Each columns contains a tissue sample.
    Returns
    -------
    array
        Disease components of T. Dimensions equal to T.
    """
    T_hat = np.copy(T)

    for i in range(T.shape[1]):
        A = nla.lstsq(N, T[:, i, None], rcond=None)[0]
        T_hat[:, i] -= np.dot(N, A).flatten()

    return np.abs(T_hat)


def significant_genes(Dcmat):
    """Threshold disease components such that only genes with high deviations are kept and
    which appear in groups of highly correlated genes.
    ----------
    Dcmat: array
        Each column contains the disease component of one sample, as computed by DSGA.
    Returns
    -------
    array
        Indices of genes, which are significant.
    """
    # Define thresholds for highly deviating genes (rows).
    perc5 = np.percentile(Dcmat, 5, axis=1)
    perc95 = np.percentile(Dcmat, 95, axis=1)
    absperc595 = np.concatenate(
        [np.abs(perc5[None, :]), np.abs(perc95[None, :])], axis=0)
    maxabs595 = np.max(absperc595, axis=0)

    # Relaxed and stringent threshold for the absolute deviation
    relaxed = np.percentile(maxabs595, 85)
    stringent = np.percentile(maxabs595, 98)

    r_ind = np.arange(Dcmat.shape[0])[maxabs595 >= relaxed]
    r_genes = Dcmat[maxabs595 >= relaxed]
    s_genes = Dcmat[maxabs595 >= stringent]

    # Keep genes which suffice the relaxed threshold and are highly correlated
    # (r > 0.6) to at least 3 genes, which suffice the stringent threshold.
    corr = np.corrcoef(r_genes, s_genes)[:r_genes.shape[0], r_genes.shape[0]:]
    r_keep = np.count_nonzero(corr > 0.6, axis=1) > 2

    return r_ind[r_keep]


def DSGA_transform(N, T):
    """Applies DSGA to normal tissue and tissue samples.
    ----------
    N: array
        Each column contains a normal tissue sample.
    T: array
        Each columns contains a tissue sample.
    Returns
    -------
    array
        Disease components of N.
    array
        Disease components of T.
    """
    N_hat = HSM(N)
    L1N = disease_comp(N_hat, N_hat)
    Dcmat = disease_comp(N_hat, T)
    genes = significant_genes(Dcmat)

    return np.concatenate([L1N[genes, :], Dcmat[genes, :]], axis=1)


def disease_comp_histogram(N, T, dim=None, filename=""):
    """Plots a histogram of MaxAbs595 of the disease components.
    ----------
    N: array
        Each column contains a normal tissue sample.
    T: array
        Each columns contains a tissue sample.
    dim: int
        Value for the dimension reduction of the HSM. If None, the goodness will be
        plotted and the user has to input the local maximum of the Wold invariant.
    filename: str
        If not the empty string, the plot will be stored under the given file name.
    Returns
    -------
    """
    N_hat = HSM(N, dim)
    L1N = disease_comp(N_hat, N_hat)
    Dcmat = disease_comp(N_hat, T)
    genes = significant_genes(Dcmat)

    perc5 = np.percentile(Dcmat, 5, axis=1)
    perc95 = np.percentile(Dcmat, 95, axis=1)
    absperc595 = np.concatenate(
        [np.abs(perc5[None, :]), np.abs(perc95[None, :])], axis=0)
    maxabs595 = np.max(absperc595, axis=0)

    relaxed = np.percentile(maxabs595, 85)
    stringent = np.percentile(maxabs595, 98)

    r_genes = len(maxabs595[maxabs595 >= relaxed])
    s_genes = len(maxabs595[maxabs595 >= stringent])

    sns_plot = sns.distplot(maxabs595, bins=100, kde=False)
    sns_plot.set_xlabel(R"$MaxAbs595$ Computed for Each Gene")
    sns_plot.set_ylabel(R"Frequency")

    # Plot vertical lines for the relaxed and stringent threshold
    r_text_pos = sns_plot.get_ylim()[1] * 0.6
    s_text_pos = sns_plot.get_ylim()[1] * 0.35

    plt.axvline(relaxed, 0, 0.5, color='green')
    plt.text(relaxed, r_text_pos, "Relaxed Threshold [85%] = {0:.2f}".format(
        relaxed) + "\n" + str(r_genes) + " Genes")
    plt.axvline(stringent, 0, 0.25, color='green')
    plt.text(stringent, s_text_pos, "Stringent Threshold [95%] = {0:.2f}".format(
        stringent) + "\n" + str(s_genes) + " Genes")

    if not filename == "":
        sns_plot.get_figure().savefig(filename)
