# The Mapper Theorem and its Application to Disease-Specific Genomic Analysis (DSGA)

## Abstract

Toplogical Data Analysis (TDA) is a fairly new topic using results from algebraic topology  to study
the structure of point cloud data sets equipped with a notion of distance.   One of the biggest
advantages of topological approaches in data science is that they are very stable under small
perturbations. The most prominent example of the TDA-toolkit is persistent homology. There, one can
construct a reasonable filtration of simplicial complexes, where the data points are the edges, and
then use homology theory to find clusters or to classify the points. See Kraft [1] for further details.

The tool we use in this work is called the mapper algorithm. The key idea is to identify local
cluster within the data and then to understand the interaction between these small clusters by
connecting them to form a graph, whose shape captures aspects of the topology of the data set.

A current goal of research in TDA is to find applications, which yield
equal or better results, than state of the art methods. Most notably this was
achieved by Nicolau, Levine, and Carlsson [2]. They used the mapper algorithm in addition
to a data analyis method called disease-specific genomic analysis (DSGA) [3] to identify a subgroup
of breast cancer patients, which was unknown before and has a favorable outcome.

The goal of this work is to explain the methods used in Nicolau, Levine, and Carlsson [2] and to
implement and apply them on a different data set. Hence, we have implemented the Disease-Specific Genomic
Analysis(DSGA) and mapper algorithm in Python and applied the Progression  Analysis of
Disease(PAD)-method defined in Nicolau, Levine, and Carlsson [2] on the breast cancer data sets in
the *Curated Microarray Database* (CuMiDa) [4].

![Graph of GSE89116 data set](results/GSE45827.png)

Figure 1: Example graph for the GSE45827 data set.

\[1\] R. Kraft, “Illustrations of data analysis using the mapper algorithm and persistent
homology”, 2016.

\[2\] M. Nicolau, A. J. Levine, and G. Carlsson, “Topology based data analysis identifies a subgroup
of breast cancers with a unique mutational profile and excellent survival.”, Proceedings of the
National Academy of Sciences of the United States of America, vol. 108 17, pp. 7265–70, 2011.

\[3\] M. Nicolau, R. Tibshirani, A.-L. Børresen-Dale, and S. Jeffrey, “Disease-specific genomic
analysis: Identifying the signature of pathologic biology”, Bioinformatics (Oxford, England),
vol. 23, pp. 957–65, May 2007.

\[4\] B. Feltes, E. Chandelier, B. Grisci, and M. Dorn, CuMiDa: An extensively curated microarray
database for benchmarking and testing of machine learning approaches in cancer research. 2019.
[Online]. Available: ![https://sbcb.inf.ufrgs.br/cumida](https://sbcb.inf.ufrgs.br/cumida).

## Installing Necessary Python Packages

```
    pip install -U -r requirements.txt
```

## Running Experiments on Breast Cancer Data

```
    python src/main.py
```

## Running Unit Tests

```
    pytest
```

## Authors

* **Sebastian Dobrzynski**
* **André Schulze**